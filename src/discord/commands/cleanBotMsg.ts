import { Context } from "../../utilities/testing.interfaces";
import { Client, Message } from "discord.js";


export const cleanChatDI = (context: Context) => {
  const prefix  = context.environment.discord.prefix;
  const { environment } = context;

  return (client: Client, msg: Message) => {
    if (!msg.author.bot && msg.content.startsWith(`${prefix}clean`)) { //se author !bot e mgs $
      msg.delete(2000)
      msg.channel.send("All the bot's messages will be deleted")
      if (true || msg.member.hasPermission("MANAGE_MESSAGES")) {
        msg.channel.fetchMessages({}).then(async messages => {
          var botMessages = msg.channel.messages.filter(m => m.author.id === "648119235125444618" || m.author.id === "647932666108313630");
          await Promise.all(botMessages.deleteAll());
        })
      }
    }
  };
};
