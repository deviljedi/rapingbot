import { Context } from "../../utilities/testing.interfaces";
import { Client, Message, ReactionCollector, CollectorFilter, ReactionCollectorOptions, Collection } from "discord.js";

export const votationDI = (context: Context) => {
  const prefix  = context.environment.discord.prefix;
  const { environment } = context;
  const { discord } = environment;
  
  const emojiToTrack1 = '👍';
  const emojiToTrack2 = '👎';

  const reactionFilter = (reaction: any) => {
    return true;
    // return reaction.name === emojiToTrack1 || reaction.name === emojiToTrack2;
  };

  let reactionCollector: ReactionCollector;


  return async (client: Client, msg: Message) => {
    if (!msg.author.bot && msg.content.startsWith(`${prefix}stopVotation`)) {
      if (reactionCollector) reactionCollector.stop("Votation ended");
    };

    if (!msg.author.bot && msg.content.startsWith(`${prefix}votation`)) {
      const content = msg.content.replace(/\s\s+/g, ' ');

      let secondi: number;
      try {
        secondi = Math.abs(parseInt(content.toString().split(" ")[1]));
        const ultimoCarattere = content[content.length - 1];
        if (ultimoCarattere === 'm') secondi *= 60;
        else if (ultimoCarattere === 'h') secondi *= 3600;
        if (!secondi) throw Error("argomento invalido");
      } catch(e) {
        secondi = 60;
      }

      const botMsg = (await msg.channel.send(`The votation begins! (seconds: ${ secondi })`)) as Message;
      reactionCollector = new ReactionCollector(botMsg, reactionFilter);

      
      var codiceInterval = setInterval(
        function botCountdown() {
          secondi -= 5;
          if (secondi === 0) {
            clearInterval(codiceInterval);
          }
          botMsg.edit(`The votation begins! (seconds: ${ secondi })`);
        }, 5000
      );


      reactionCollector.on('end', async (collected: Collection<any, any>, reason: any) => {
        
        await msg.channel.send("The votation ended!")
        const collectedArr = collected.array();

        const strDaInviare = collectedArr.reduce((acc, reaction) => acc + ` ${ reaction._emoji.name }: ${ reaction.count }`, "Result :");

        msg.channel.send(strDaInviare);
      });

      botMsg.react(emojiToTrack1);
      botMsg.react(emojiToTrack2);

      setTimeout(() => {
        reactionCollector.stop('Votation ended');
      }, secondi * 1000);
      
    }
  };
};
