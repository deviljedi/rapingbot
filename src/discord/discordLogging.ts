import { Context } from "../utilities/testing.interfaces";
import { Client } from "discord.js";

export const discordLoggingDI = (context: Context) => {

  return (client: Client) => {
    client.on('warn',  console.warn);
    client.on('error', console.error);
  }
}