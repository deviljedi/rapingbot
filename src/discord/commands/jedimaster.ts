import { Context } from "../../utilities/testing.interfaces";
import { Client, Message } from "discord.js";

export const jedimasterDI = (context: Context) => {
  const prefix  = context.environment.discord.prefix;
  const { environment } = context;
  const { discord } = environment;

  return (client: Client, msg: Message) => {
    if (!msg.author.bot && msg.content.startsWith(`${prefix}jedimaster`)) {
      if (msg.author.id !== discord.adminID) {
        msg.channel.send("NON SEI DEGNO!!")
        msg.member.kick().then(() => msg.channel.send(`Kicked ${msg.member.displayName}`))
        .catch(console.error);
      } else {
        msg.channel.send("Hello Wiktor SUPREME JEDI MASTER!!!");
      }
    }
  };
};
