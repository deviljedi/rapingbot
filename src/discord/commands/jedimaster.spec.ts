import "jest";
import { jedimasterDI } from './jedimaster';

describe('jedimaster', () => {
    const context = {
        environment: {
            discord: {
                prefix: '$',
                adminID: '1'
            }
        }
    };
    const msg = {
        author: {
            bot: false,
            id: context.environment.discord.adminID
        },
        content: "$jedimaster",
        channel: {
            send: (inputJedimaster: string) => jedimaster = inputJedimaster
        }
    };
    const client = {
        on: (eventName: string, handler: any) => {
            handler(msg);
        }
    };

    let jedimaster: string;

    it("if Admin, send special jedimaster", () => {
        jedimasterDI(context as any)(client as any);

        expect(jedimaster).toBe("Ciao Wiktor CAPO SUPREMO!!!");
    });
});
