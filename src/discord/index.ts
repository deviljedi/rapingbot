import { Context }          from "../utilities/testing.interfaces";
import { initDiscordDI }    from './initDiscord';
import { discordLoggingDI } from "./discordLogging";
import { botchatDI }        from './commands/botchat';
import { greetingDI }       from "./commands/greeting";
import { bulkDeleteChatDI } from "./commands/bulkDeleteChat";
import { addRolesDI }       from "./commands/addRole";
import { jedimasterDI }     from "./commands/jedimaster";
import { cleanChatDI }      from "./commands/cleanBotMsg";
import { commandListDI }    from "./commands/commandsList";
import { randomDI }         from "./commands/random";
import { persecuteDI }      from "./commands/perseguita";
import { sendImageDI }      from "./commands/sendImage";
import { registerDI }       from "./commands/register";
import { grpPhotoDI }       from "./commands/grpPhoto";
import { votationDI }       from "./commands/votazione";
import { botChannelDI }     from "./commands/botChannel";
import { inviteDI }         from "./commands/invite";
import { pardonDI }         from "./commands/pardon";
import { colorDI }          from "./commands/color";
import { imageDI }          from "./commands/images";
//import { soundboardDI }   from "./commands/soundBoard";
import { dadoDI }           from "./commands/dado";
import { csrDI }            from "./commands/csreleases";

export const discordDI = (context: Context) => {

  context.persecuzioni = [];

  return async () => {
    const client = await initDiscordDI(context)();

    const discordLogging  = discordLoggingDI(context);
    const botchat         =        botchatDI(context);
    const greeting        =       greetingDI(context);
    const bulkDeleteChat  = bulkDeleteChatDI(context);
    const addRoles        =       addRolesDI(context);
    const jedimaster      =     jedimasterDI(context);
    const cleanChat       =      cleanChatDI(context);
    const commandList     =    commandListDI(context);
    const random          =         randomDI(context);
    const persecute       =      persecuteDI(context);
    const sendImage       =      sendImageDI(context);
    const register        =       registerDI(context);
    const grpPhoto        =       grpPhotoDI(context);
    const votation        =       votationDI(context);
    const botChannel      =     botChannelDI(context);
    const invite          =         inviteDI(context);
    const pardon          =         pardonDI(context);
    const color           =          colorDI(context);
    const image           =          imageDI(context);
//  const soundboard      =     soundboardDI(context);
    const dado            =           dadoDI(context);
    const csr            =             csrDI(context);

    client.on('message', async msg => {

      discordLogging( client     );
      botchat(        client, msg);
      greeting(       client, msg);
      bulkDeleteChat( client, msg);
      addRoles(       client, msg);
      jedimaster(     client, msg);
      cleanChat(      client, msg);
      commandList(    client, msg);
      random(         client, msg);
      persecute(      client, msg);
      sendImage(      client, msg);
      register(       client, msg);
      grpPhoto(       client, msg);
      votation(       client, msg);
      botChannel(     client, msg);
      invite(         client, msg);
      pardon(         client, msg);
      color(          client, msg);
      image(          client, msg);
  //  soundboard(     client, msg);
      dado(           client, msg);
      csr(            client, msg);
    });


    
    return client;
  }
};
