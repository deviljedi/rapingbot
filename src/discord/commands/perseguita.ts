import { Context } from "../../utilities/testing.interfaces";
import { Client, Message } from "discord.js";
import * as fs from 'fs';

import * as conf from "./conf.json";

export const persecuteDI = (context: Context) => {
  const prefix  = context.environment.discord.prefix;

  try {
    context.persecuzioni = JSON.parse(fs.readFileSync('./db.json').toString()).persecuzioni || []; 
  } catch (e) {
    console.warn('lettura di db.json è andata male');
    context.persecuzioni = [];
  }

  return (client: Client, msg: Message) => {

    const persecuzione = context.persecuzioni.find((pers: [string, string, number]) => pers[0] === msg.author.id);

    if (!msg.author.bot && msg.content.startsWith(`${prefix}persecute`)) {
      const content = msg;
      const [ comando, authID, messaggio, tempo] = content.toString().split(" ");
      var autoreID = authID; 
      var mess = messaggio; 
      var time = parseInt(tempo);
      if (Number.isNaN(time)) time = 0;
      var tuple = [autoreID, mess, time] as [string, string, number];
      context.persecuzioni.push(tuple);
      const data = JSON.stringify({ persecuzioni: context.persecuzioni });//scrive nel file json
      fs.writeFileSync('./db.json', data);
    } else if (persecuzione) {
      const time = persecuzione[2];
      setTimeout(function(){
        msg.reply(persecuzione[1]);
      }, time);
    }
  };
};
