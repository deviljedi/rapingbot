const { Client, Util } = require('discord.js');
const { token, prefix } = require('../discordCredential.json');
const Canvas = require('canvas');

const client = new Client({ disableEveryone: true });
const adminID = '354933555849199633';

client.on('warn', console.warn);

client.on('error', console.error);

client.on('message', async msg => {
    if (message.content === '!join') {
		client.emit('guildMemberAdd', message.member || await message.guild.fetchMember(message.author));
	}
});

client.on('ready', () => {
    client.user.setPresence({
        game: {
            name: 'something',
            type: 'playing'
        },
        status: 'online'
    })
    console.log('Baby Yoda OP!');
});

// client.on('guildMemberAdd', async member => {
// 	const channel = member.guild.channels.find(ch => ch.name === 'member-log');
// 	if (!channel) return;

//     const canvas = Canvas.createCanvas(700, 250);
//     const ctx = canvas.getContext('2d');
    
//     const background = await Canvas.loadImage('project\\logo\\darthmaul-test.jpg');
// 	ctx.drawImage(background, 0, 0, canvas.width, canvas.height);
// });

client.login(token);
