import { Context } from "../../utilities/testing.interfaces";
import { Client, Message } from "discord.js";
import * as fs from 'fs';



export const botChannelDI = (context: Context) => {
  const prefix  = context.environment.discord.prefix;
  const channelIDs = './channelIDs.json';
  const prefissi = [`$`, `;;`, `!`];

  let channelMessage: string[];
  try {
    channelMessage = JSON.parse(fs.readFileSync(channelIDs).toString()).channelMessage || [];      
  } catch (e) {
    console.error(e);
    
    console.warn('lettura di channelIDs.json è andata male');
    channelMessage = [];
  };
  
  return async (client: Client, msg: Message) => {

    // const persecuzione = channelMessage.find((pers: [string, string]) => pers[0] === msg.author.id);


    if (!msg.author.bot && msg.content.startsWith(`${prefix}botchannel`)) { 
      const content = msg;
      const [ comando, channelID ] = content.toString().split(" ");
      channelMessage.push(channelID);
      const data = JSON.stringify({ channelMessage });//scrive nel file json
      fs.writeFileSync(channelIDs, data);
    } else if (prefissi.some(prefisso => msg.content.startsWith(prefisso)) && !channelMessage.some(chl => chl === msg.channel.id)) {
      if (msg.channel.lastMessage && !msg.author.bot) {
        const messaggio = (await msg.reply('don\'t write here, go in bot-commands')) as Message;
        setTimeout(() => {
          messaggio.delete();
        }, 3000);
      };
    }
  };
};
