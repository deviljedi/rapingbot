import { Context } from "../../utilities/testing.interfaces";
import { Client, Message } from "discord.js";
import * as fs from 'fs';
import * as conf from "./conf.json";



export const registerDI = (context: Context) => {
  const prefix  = context.environment.discord.prefix;

  let registeredUserIDs: [string, string][];
  
  const filePath = './registeredUserIDs.json';
  try {
    registeredUserIDs = JSON.parse(fs.readFileSync(filePath).toString()).registeredUserIDs || [];
  } catch (e) {
    console.warn('lettura di registeredUserIDs.json è andata male');
    registeredUserIDs = [];
  }

  
  return (client: Client, msg: Message) => {
      if (!msg.author.bot && msg.content.startsWith(`${prefix}register`)) {
      const authorID = msg.author.id;
      const authorNickName = msg.author.username;

      registeredUserIDs.push([authorID, authorNickName]);
      const strToWrite = JSON.stringify({ registeredUserIDs }, null, 2);
      fs.writeFileSync(filePath, strToWrite);
    }
  };
};

