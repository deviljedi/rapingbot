const development = process.env.NODE_ENV === 'development';
const environmentPath = `../../environment/environment${ development ? '': '.prod' }.json`;

const environment = require(environmentPath);

import { discordDI } from './discord';
import { Context } from './utilities/testing.interfaces';

const context: Context = {
  environment
};

(async () => {
  const { token } = environment.discord;

  const client = await discordDI(context)();

  client.login(token);
})();
