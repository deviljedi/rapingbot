import { Context } from "../../utilities/testing.interfaces";
import { Client, Message } from "discord.js";

export const csrDI = (context: Context) => {
  const prefix  = context.environment.discord.prefix;
  const { environment } = context;
  const { discord } = environment;

  return (client: Client, msg: Message) => {
    if (!msg.author.bot && msg.content.startsWith(`${prefix}csr`)) {
      msg.channel.send("!csreleases");
    }
  };
};
