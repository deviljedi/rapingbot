import "jest";
import { greetingDI } from './greeting';

describe('greeting', () => {
    const context = {
        environment: {
            discord: {
                prefix: '$',
                adminID: '1'
            }
        }
    };
    const msg = {
        author: {
            bot: false,
            id: context.environment.discord.adminID
        },
        content: "$ciao",
        channel: {
            send: (inputGreeting: string) => greeting = inputGreeting
        }
    };
    const client = {
        on: (eventName: string, handler: any) => {
            handler(msg);
        }
    };

    let greeting: string;

    it("if Admin, send special greeting", () => {
        greetingDI(context as any)(client as any);

        expect(greeting).toBe("Ciao Wiktor CAPO SUPREMO!!!");
    });
});
