import { Context } from "../../utilities/testing.interfaces";
import { Client, Attachment, User, Message } from "discord.js";
import * as Canvas from "canvas";
import * as fs from 'fs';
import * as Request from 'request';


function download (uri: string, filename: string, callback: any){
  Request.head(uri, function(err: any, res: any, body: any){
    Request(uri).pipe(fs.createWriteStream(filename)).on('close', callback);
  });
};

export function tag2ID(str: string): string {
  if (str.startsWith('<@&') || str.startsWith('<@!')) {
    return str.substring(3, str.length - 1);
  } else if (str.startsWith('<@')) {
    return str.substring(2, str.length - 1);
  }
}
// User: \@user => <@user_id> 
// User with nickname: \@user => <@!user_id>
// Role: \@role => <@&role_id>
// Channel: \#channel => <#channel_id>
// Emoji: \:emoji: => <:emoji_name:emoji_id>
// Animated Emoji: \:emoji: => <a:emoji_name:emoji_id>


export const sendImageDI = (context: Context) => {
  const prefix  = context.environment.discord.prefix;
  const { environment } = context;
  const { discord } = environment;

   return async (client: Client, msg: Message) => {
    if (!msg.author.bot && msg.content.startsWith(`${prefix}logo`)) {
      const canvas = Canvas.createCanvas(700, 700);
      const ctx = canvas.getContext('2d');
      const content = msg.content.replace(/\s\s+/g, ' ');
      const [ comando, authIDOrTag, img] = content.toString().split(" ");
      const autoreID = tag2ID(authIDOrTag) ? tag2ID(authIDOrTag) : authIDOrTag;

      const user:User = await client.fetchUser(autoreID);
      let path = user.avatarURL || "C:/discordrape/rapingbot/assets/darthVader.jpg";

      const fileName = path.split('/').pop().split('?')[0];
      const filePath = './assets/' + fileName;


      download(path, filePath, async () => {
        const background = await Canvas.loadImage(filePath);
        ctx.drawImage(background, 0, 0, canvas.width, canvas.height);

        let deco;
        try {
          deco = await Canvas.loadImage('./assets/' + (img ?? 'rainbow') + '.png');
        } catch (e) {
          console.error(e);
          
          deco = await Canvas.loadImage('./assets/rainbow.png');
        }
        ctx.drawImage(deco, 0, 0, canvas.width, canvas.height);
        
        const attachment = new Attachment(canvas.toBuffer(), fileName);

        await msg.channel.send(``, attachment);
      });        
    }
  };
};
