import { Context } from "../../utilities/testing.interfaces";
import { Client, Message } from "discord.js";


export const bulkDeleteChatDI = (context: Context) => {
  const prefix  = context.environment.discord.prefix;
  const { environment } = context;

  return (client: Client, msg: Message) => {
    if (!msg.author.bot && msg.content.startsWith(`${prefix}delete`)) {
      if (msg.member.hasPermission("MANAGE_MESSAGES")) {
        msg.channel.fetchMessages({}).then(() => {
          const contenuto = msg;
          const [ comando, maxStr ] = contenuto.toString().split(" ");
          let max = Math.abs(parseInt(maxStr));
          if (Number.isNaN(max)) max = 10;
          msg.channel.bulkDelete(max)
        });
      }
    }
  };
};
