import { Context } from "../../utilities/testing.interfaces";
import { Client, Message } from "discord.js";

export const commandListDI = (context: Context) => {
  const prefix  = context.environment.discord.prefix;

  return (client: Client, msg: Message) => {
    if (!msg.author.bot && msg.content.startsWith(`${prefix}commands`)) {
      msg.channel.send(`
      Here is the list of commands:
  $hello
  $invite
  $delete [maxNum] (bulk deletes a number of messages [defauld = 10])
  $clean (removes bot's messages)
  $color 
  $dado [how many faces you want the dice to have] [how many times it roll]
  $groupPhoto [@tag the people you want in the photo]
  $jedimaster [DON'T USE IT!]
  $persecute [userID] [message] [time(ms)]
  $pardon [userID]
  $image [something you want an image of]
  $random [maxNum]
  $register
  $logo [ID or @tag] [image name]
  $votation [time(seconds)] [numberm(minutes)] [numberh(hours)] 
  $botchannel [textChannelID]
      `)
    }
  };
};
