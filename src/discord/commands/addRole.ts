import { Context } from "../../utilities/testing.interfaces";
import { Client, Message } from "discord.js";


export const addRolesDI = (context: Context) => {
  const prefix  = context.environment.discord.prefix;
  const { environment } = context;
  const { discord } = environment;

  return (client: Client, msg: Message) => {
    if (!msg.author.bot && msg.content.startsWith(`${prefix}addRole`)) {
      msg.member.addRole("535736995478110219"); //mette il ruolo Gea

    }
  };
};
