import { Context } from "../../utilities/testing.interfaces";
import { Client, Message } from "discord.js";

export const randomDI = (context: Context) => {
  const prefix  = context.environment.discord.prefix;

  return (client: Client, msg: Message) => {
    if (!msg.author.bot && msg.content.startsWith(`${prefix}random`)) {
      const content = msg;
      const [ comando, maxStr ] = content.toString().split(" ");
      let max = parseInt(maxStr);

      if (typeof max !== 'number' || Number.isNaN(max)) max = 10;

      msg.channel.send(Math.round(Math.random() * max))
    }
  };
};
