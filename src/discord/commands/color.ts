import { Context } from "../../utilities/testing.interfaces";
import { Client, Message } from "discord.js";

export const colorDI = (context: Context) => {
  const prefix  = context.environment.discord.prefix;
  const { environment } = context;
  const { discord } = environment;


  function colori(hexColor:any) {
    var result = /^#?([a-f\d]{2})([a-f\d]{2})([a-f\d]{2})$/i.exec(hexColor);
    return result ? {
      r: parseInt(result[1], 16),
      g: parseInt(result[2], 16),
      b: parseInt(result[3], 16)
    } : null;
  }
  
  return (client: Client, msg: Message) => {
    if (!msg.author.bot && msg.content.startsWith(`${prefix}color`)) {
      const content = msg;
      const [ comando, hexColor] = content.toString().split(" ");
      
      const a = colori(hexColor);
      const b = JSON.stringify(a);
      msg.channel.send(b);
    }
  };
};
