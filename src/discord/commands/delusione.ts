export function inutile( str: string ) {
  return str.substring(['<@&', '<@!', '<@'].find(p => str.startsWith(p)).length, str.length - 1);
} 
