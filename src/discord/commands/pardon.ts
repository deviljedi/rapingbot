import { Context } from "../../utilities/testing.interfaces";
import { Client, Message } from "discord.js";
import * as fs from 'fs';


//no more persecuted (only admin can)
export const pardonDI = (context: Context) => {
  const prefix  = context.environment.discord.prefix;
  const { environment } = context;
  const { discord } = environment;
  
  const path = './db.json';
  
  try {
    context.persecuzioni = JSON.parse(fs.readFileSync(path).toString()).persecuzioni || []; 
  } catch (e) {
    console.warn('lettura di db.json è andata male');
    context.persecuzioni = [];
  }
  
  return (client: Client, msg: Message) => {
    if (msg.author.id === discord.adminID && !msg.author.bot && msg.content.startsWith(`${prefix}pardon`)) {
      const content = msg;
      const [ comando, perdonatoID] = content.toString().split(" ");
      context.persecuzioni = context.persecuzioni.filter(per => per[0] !== perdonatoID);

      fs.writeFileSync(path, JSON.stringify({persecuzioni: context.persecuzioni}));
    }
  };
};
