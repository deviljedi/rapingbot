import * as Environment from '../../../environment/environment.json';

export interface Context {
  environment: typeof Environment;
  persecuzioni?: [string, string, number][];
}
