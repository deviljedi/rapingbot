const { Client, Util } = require('discord.js');
const { TOKEN, PREFIX, GOOGLE_API_KEY } = require('./config');
const YouTube = require('simple-youtube-api');
const ytdl = require('ytdl-core');

const client = new Client({ disaleEveryone: true });
const youtube = new YouTube(GOOGLE_API_KEY);
const queue  = new Map();

const adminID = '354933555849199633';

client.on('warn', console.warn);

client.on('error', console.error);

client.on('ready', () => {
    client.user.setPresence({
        game: {
            name: 'the community',
            type: 'LISTENING'
        },
        status: 'online'
    })
    console.log('Wiktor is the best! Genni is the Jedi sensei!'); 
});

client.on('disconnect', () => console.log('I just disconnected, making sure you know, I will reconnect now...'));

client.on('reconnecting', () => console.log('I am reconnecting now!'));

client.on('message', async msg => {
    if (msg.author.bot) return undefined;
    
    
    if (!msg.content.startsWith(PREFIX)) return undefined;
    //console.log({msg});
    // str = "$gnegne"  =>  str.split(' ')  => vet = ["w!gnegne"]  =>  vet[1] => undefined
    const args = msg.content.split(' ');
    const searchString = args.slice(1).join(' ');
    const serverQueue = queue.get(msg.guild.id);

    //if (msg.content.startsWith(`${PREFIX}cleanErr`)) {
    //     for (msg.) {
    //         msg.content.delete()
    //     }
    // }

    if (msg.content.startsWith(`${PREFIX}play`)) {
        if (msg.author.id === '359778189842579456' && msg.content.id === '359712772776525824') return msg.channel.send('PAR O CAZZ!'); //insulti per Gippi :D
        const voiceChannel = msg.member.voiceChannel;
        if (!voiceChannel) return msg.channel.send('Err: I can\'t connect if you aren\'t in a voice channel.');
        const permissions = voiceChannel.permissionsFor(msg.client.user);
        if (!permissions.has('CONNECT')) {
            return msg.channel.send('Err: I can\'t connect to your channel if I dont have enough permissions!')
        }
        if (!permissions.has('SPEAK')) {
            return msg.channel.send('Err: I can\'t speak if you don\'t give me enough permissions!');
        }
        
        const url = args[1].replace(/<(.+)>/g, '$1');
        if (url.match(/^https?:\/\/(www.youtube.com|youtube.com)\/playlist(.*)$/)) {
            // (new (require('simple-youtube-api'))(require('./config').GOOGLE_API_KEY)).getPlaylist("https://www.youtube.com/watch?v=E2Q52cVx7Bo&list=PL6305F709EB481224").then(x => console.log({x}));
            // (new (require('simple-youtube-api'))(require('./config').GOOGLE_API_KEY)).getPlaylistByID("PL6305F709EB481224").then(x => console.log({x}));
            const playlist = await youtube.getPlaylist(url);
            const videos = await playlist.getVideos();
            //console.log({videos});
            for (const video of Object.values(videos)) {
                const video2 = await youtube.getVideoByID(video.id); // eslint disable line no-await in loop
                await handleVideo(video2, msg, voiceChannel, true) // eslint disable line no-await in loop
            }
            
        } else {
            try {
                var video = await youtube.getVideo(url);
            } catch (err) {
                try {
                    var videos = await youtube.searchVideos(searchString, 1);
                    var video  = await youtube.getVideoByID(videos[0].id);
                } catch (error) {
                    console.error(error);
                    return msg.channel.send('Err: I could not obtain any search results.');
                }
            }
            
            return handleVideo(video, msg, voiceChannel);
        }
    } else if (msg.content.startsWith(`${PREFIX}skip`)) {
        if (!msg.member.voiceChannel) return msg.channel.send('Err: You aren\'t in a voice channel!');
        if (!serverQueue) return msg.channel.send('Err: Here is nothing playing that I could skip for you.');
        serverQueue.connection.dispatcher.end('Skip command has been used!');
        return undefined;
    } else if (msg.content.startsWith(`${PREFIX}leave`)) {
        if (!msg.member.voiceChannel) return msg.channel.send('Err: You aren\'t in a voice channel!');
        if (!serverQueue) {
            msg.member.voiceChannel.leave();
        } else {
            serverQueue.songs = [];
            serverQueue.connection.dispatcher.end('Leave command has been used!');
        }
        
        return undefined;
    } else if (msg.content.startsWith(`${PREFIX}volume`)) {
        if (!msg.member.voiceChannel) return msg.channel.send('Err: You aren\'t in a voice channel!');
        if (!serverQueue) return msg.channel.send('Err: Here is nothing playing.');
        if (!args[1]) return msg.channel.send(`The current volume is: ${serverQueue.volume}`);
        serverQueue.connection.dispatcher.setVolumeLogarithmic(args[1] / 5);
        return undefined;
    } else if (msg.content.startsWith(`${PREFIX}np`)) {
        if (!serverQueue) return msg.channel.send('Err: Here is nothing playing.');
        return msg.channel.send(`Now playing: **${serverQueue.songs[0].title}**`);
    } else if (msg.content.startsWith(`${PREFIX}queue`)) {
        if (!serverQueue) return msg.channel.send('Err: Here is nothing playing.');
        return msg.channel.send(`
        __**Song queue:**__
        
        ${serverQueue.songs.map(song => `**-** ${song.title}`).join('\n')}
        
        **Now playing:** ${serverQueue.songs[0].title}
        `);
    } else if (msg.content.startsWith(`${PREFIX}pause`)) {
        if (serverQueue && serverQueue.playing) {
            serverQueue.playing = false;
            serverQueue.connection.dispatcher.pause();
            return msg.channel.send('Err: Music paused, resume with $resume.');
        }
        return msg.channel.send('Err: There is nothing playing.');
    } else if (msg.content.startsWith(`${PREFIX}resume`)) {
        if (serverQueue && !serverQueue.playing) {
            serverQueue.playing = true;
            serverQueue.connection.dispatcher.resume();
            return msg.channel.send('Music resumed!');
        }
        return msg.channel.send('Err: I can\'t resume if there is nothing playing.');
    } else if (msg.content.startsWith(`${PREFIX}exec`) && msg.author.id === adminID) {
        const args = msg.content.split(' ');
        const comando = args.slice(1).join(' ');
        const risposta = eval(comando);
        return msg.channel.send(risposta);
    } else if (msg.content.startsWith(`${PREFIX}clear`)) {
        if (!serverQueue) return msg.channel.send('Err: I can\'t clear a not existing queue.');
        serverQueue.songs = [];
    } else if (msg.content.startsWith(`${PREFIX}commands`)) { //**${serverQueue.songs[0].title}**
        return msg.channel.send(`
    __**These are all my commands**__
    **$play [url | search_term]**
    **$skip**
    **$leave**
    **$pause**
    **$resume**
    **$volume [from 1 to 5]**
    **$np **(Now Playing)
    **$queue**
    **$clear** (clears the queue)
    **$join**
    **$invite** (invite in your discord server)
    **$delete** (clears messages from the text channel)
    `);
} else if (msg.content.startsWith(`${PREFIX}join`)) {
    const voiceChannel = msg.member.voiceChannel;
        if (!voiceChannel) return msg.channel.send('Err: I can\'t connect if you aren\'t in a voice channel.');
        const permissions = voiceChannel.permissionsFor(msg.client.user);
        if (!permissions.has('CONNECT')) {
            return msg.channel.send('Err: I can\'t connect to your channel if I dont have enough permissions!');
        } else {voiceChannel.join()}
    } else if (msg.content.startsWith(`${PREFIX}invite`)) {
        msg.channel.send('https://discord.now.sh/534827638506913794')
    } else if (msg.content.startsWith(`${PREFIX}Who is your God?`)) {
        msg.channel.send('Sir Wiktor19051511')
    } else if (msg.content.startsWith(`${PREFIX}delete`)) {
        if (msg.member.hasPermission("MANAGE_MESSAGES")) {
            msg.channel.fetchMessages({}).then(
                msg.channel.bulkDelete(25)
            )
        }
    } else if (msg.content.startsWith(`${PREFIX}pasta`)) {
        msg.channel.send('Tasto per la PASTA')
    }
    return undefined;
});

async function handleVideo(video, msg, voiceChannel) {
    const serverQueue = queue.get(msg.guild.id);
    //console.log(video);
    const song = {
        id: video.id,
        title: video.title,
        url: `https://www.youtube.com/watch?v=${video.id}`
    };
    if (!serverQueue) {
        const queueConstruct = {
            textChannel: msg.channel,
            voiceChannel: voiceChannel,
            connection: null,
            songs: [],
            volume: 5,
            playing: true
        };
        queue.set(msg.guild.id, queueConstruct);  //?
        
        queueConstruct.songs.push(song);

        try {
            var connection = await voiceChannel.join();
            queueConstruct.connection = connection;
            play(msg.guild, queueConstruct.songs[0]);
        } catch (error) {
            //console.error(`I could not join the voice channel: ${error}`);
            queue.delete(msg.guild.id);
            return msg.channel.send(`Err: I could not join the voice channel: ${error}`);
        }
    } else {
        serverQueue.songs.push(song);
        return msg.channel.send(`**${song.title}** has been added to the queue`);
    }
    return undefined;
}

function play(guild, song) {
    const serverQueue = queue.get(guild.id);
    // console.log(serverQueue)
    
    if (!song) {
        serverQueue.voiceChannel.leave();
        queue.delete(guild.id);
        return;
    }
    // console.log(serverQueue.songs);

    const dispatcher = serverQueue.connection.playStream(ytdl(song.url))
        .on('end', reason => {
            if (reason === 'Err: Stream is not generating quickly enough.') //console.log('Song ended.');
            // console.log(reason);
            serverQueue.songs.shift();
            play(guild, serverQueue.songs[0]);
        })
        .on('error', error => console.error(error));
    dispatcher.setVolumeLogarithmic(serverQueue.volume / 5);

    serverQueue.textChannel.send(`Now playing: **${song.title}**`);
}


client.login(TOKEN);