import { Context } from "../../utilities/testing.interfaces";
import { Client, Message } from "discord.js";

export const inviteDI = (context: Context) => {
  const prefix  = context.environment.discord.prefix;
  const { environment } = context;
  const { discord } = environment;

  return (client: Client, msg: Message) => {
    if (!msg.author.bot && msg.content.startsWith(`${prefix}invite`)) {
      msg.channel.send("Here is the link to invite me in your server: https://discordapp.com/api/oauth2/authorize?client_id=647932666108313630&permissions=8&scope=bot");
    }
  };
};
