import { Context } from "../../utilities/testing.interfaces";
import { Client, Message } from "discord.js";
import Unsplash, { toJson } from 'unsplash-js';
import fetch from 'node-fetch';
// @ts-ignore
global.fetch = fetch;


export const imageDI = (context: Context) => {
  const prefix  = context.environment.discord.prefix;
  const { environment } = context;
  const { discord } = environment;
  const unsplash = new Unsplash({ accessKey: "1719208d65dc0a7405b7c263a49c30325606c0dab4b5f4cd009d1cc49c09b142" });
  
  
  return (client: Client, msg: Message) => {

    if (!msg.author.bot && msg.content.startsWith(`${prefix}image`)) {
      const content = msg;
      const [ comando, imgName ] = content.toString().split(" ");
      
      var image = unsplash.search.photos(imgName, 1, 10, { orientation: "portrait" })
        .then(toJson)
        .then(json => {
          const urlImage = json.results[0].urls.regular;
          msg.channel.send(urlImage);
          // console.log(JSON.stringify(json.results[0], null, 2));
          // console.log(JSON.stringify(json, null, 2));
        });

    } else if (!msg.author.bot && msg.content.startsWith(`${prefix}casualimage`)) {
      const content = msg;
      const [ comando, imgNome ] = content.toString().split(" ");

      unsplash.photos.getRandomPhoto({ query:imgNome })
      .then(toJson)
      .then(res => {
        var shef = res.urls.full;
        msg.channel.send(shef);
      });
    }
  };
};
