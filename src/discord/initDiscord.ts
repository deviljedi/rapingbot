import { Context } from "../utilities/testing.interfaces";
import { DiscordConfig } from "./discord.interfaces";

import { Client, PresenceData } from 'discord.js';




export const initDiscordDI = (context: Context) => {
  const { environment } = context;
  const { presence } = environment.discord;

  return async () => {
    const client = new Client({ disableEveryone: true });

    client.on('ready', async () => {
      await client.user.setPresence(presence as PresenceData)
      console.log('Baby Yoda OP!');
    });

    return client;
  }
};
