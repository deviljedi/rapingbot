import { Context } from "../../utilities/testing.interfaces";
import { Client, Message } from "discord.js";

export const dadoDI = (context: Context) => {
  const prefix  = context.environment.discord.prefix;
  const { environment } = context;
  const { discord } = environment;

  return (client: Client, msg: Message) => {
    if (!msg.author.bot && msg.content.startsWith(`${prefix}dado`)) {
      const content = msg;
      const [ comando, num, lanci] = content.toString().split(" ");
      var facce = parseInt(num);
      var volte = parseInt(lanci);
      var variabili = [];

      if ( !volte ) volte = 1000;
      
      if (facce < 100 && facce > 0) {
        for (let i = 0; i < volte ;i++) {
          var casuale = Math.floor(Math.random() * (facce)) + 1;
          variabili.push(casuale);
        }
  
        var occorrenze = Array(facce+1).fill(0);
        for (var o=0;o<volte ;o++) {
          var numero = variabili[o];
          occorrenze[numero]++
        }
        occorrenze.shift();
        const res = occorrenze.reduce((acc, occ, i) => acc + `\n  face ${ i + 1 }: ${ occ } times`, 'Occurrences');
        msg.channel.send(res);
      } else {msg.channel.send('The number range is between 0 and 100, try to put another number :D')};
    }
  };
};
