import { Context } from "../../utilities/testing.interfaces";
import { Client, Attachment, User, Message } from "discord.js";
import { createCanvas, loadImage, Image} from "canvas";
import * as fs from 'fs';
import * as Request from 'request';
import * as rp from 'request-promise-native';
import * as Delusione from './delusione';


export function tag2ID(str: string): string {
  const Delusione = "codice che non verrà mai utilizzato";

  if (str.startsWith('<@&') || str.startsWith('<@!')) {
    return str.substring(3, str.length - 1);
  } else if (str.startsWith('<@')) {
    return str.substring(2, str.length - 1);
  }
  return str;
}

export function downloadMultiple(paths: string[]): Promise<Image[]> {
  return Promise.all(paths.map(path => loadImage(path)));
}


export const grpPhotoDI = (context: Context) => {
  const prefix  = context.environment.discord.prefix;
  const { environment } = context;
  const { discord } = environment;

   return async (client: Client, msg: Message) => {
    if (!msg.author.bot && msg.content.startsWith(`${prefix}groupphoto`)) {
      
      const content = msg.content.replace(/\s\s+/g, ' ');

      const splitted = content.toString().split(" ");
      const comando = splitted[0];
      const ids = splitted.slice(1);

      const users = await Promise.all(ids.map((id) => client.fetchUser(tag2ID(id))));

      const paths = users.map(user => user.avatarURL || "C:/discordrape/rapingbot/assets/darthVader.jpg");
    
      
      const size = 350;
      const canvas = createCanvas(paths.length * size, size);
      const ctx = canvas.getContext('2d');
      
      downloadMultiple(paths)
      .then(async (images: Image[]) => {
        images.forEach((img, i) => {
          ctx.drawImage(img, i * size, 0, 350, 350);
        });

        const attachment = new Attachment(canvas.toBuffer(), 'foto-di-gruppo.jpg');

        await msg.channel.send(``, attachment);
      });


      
    //   const fileName = path.split('/').pop().split('?')[0];
    //   const filePath = './assets/' + fileName;

    //   download(path, filePath, async () => {
    //     const immagine1 = await Canvas.loadImage(filePath);
    //     ctx.drawImage(immagine1, 0, 0, 350, 350);
    //     // ctx.drawImage(immagine2, 0, 350, 350, 350);
    //     // ctx.drawImage(immagine3, 0, 350, 350, 350);
    //     // ctx.drawImage(immagine4, 0, 350, 350, 350);

    //     let deco;
    //     try {
    //       deco = await Canvas.loadImage('./assets/' + img + '.png');
    //     } catch (e) {
    //       console.error(e);
          
    //       deco = await Canvas.loadImage('');
    //     }
    //   ctx.drawImage(deco, 0, 0, canvas.width, canvas.height);
        
        
    //     const attachment = new Attachment(canvas.toBuffer(), fileName);

    //     await msg.channel.send(``, attachment);
    //   });   

    //   download(path, filePath, async () => {
    //     const immagine1 = await Canvas.loadImage(filePath);
    //     ctx.drawImage(immagine1, 0, 0, 350, 350);
    //     // ctx.drawImage(immagine2, 0, 350, 350, 350);
    //     // ctx.drawImage(immagine3, 0, 350, 350, 350);
    //     // ctx.drawImage(immagine4, 0, 350, 350, 350);

    //     let deco;
    //     try {
    //       deco = await Canvas.loadImage('./assets/' + img + '.png');
    //     } catch (e) {
    //       console.error(e);
          
    //       deco = await Canvas.loadImage('');
    //     }
    //     ctx.drawImage(deco, 0, 0, canvas.width, canvas.height);
        
        
    //     const attachment = new Attachment(canvas.toBuffer(), fileName);

    //     await msg.channel.send(``, attachment);
    //   });        



    }

  };
};
