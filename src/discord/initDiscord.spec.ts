import { initDiscordDI } from './initDiscord';

import * as environment from '../../../environment/environment.json';
import { Context } from '../utilities/testing.interfaces';
import { Client } from 'discord.js';


const context: Context = { environment };
const { token } = context.environment.discord

describe(`initDiscord`, () => {
  const initDiscord = initDiscordDI(context);
  let client: Client;
  
  const mockFn = jest.fn();

  it(`init`, async () => {
    let twoSteps = 0;
    client = await initDiscord();

    client.on(`ready`, mockFn);

    await client.login(token);
    expect(mockFn).toBeCalledTimes(1);
  });

  afterEach(async () => {
    client.off('ready', mockFn);
  });

  afterAll(async () => {
    await client.destroy();
  });

});

